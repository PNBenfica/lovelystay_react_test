import React from 'react'

import KeyValueTR from './KeyValueTR'

const UserRepoInfo = props => {

  const { name, description } = props

  return (
    <tbody>
      <KeyValueTR k="name" v={name}/>
      <KeyValueTR k="description" v={description}/>
    </tbody>
  )
}

export default UserRepoInfo
