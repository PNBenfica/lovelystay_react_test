import React from 'react'

import UserProfileInfo from './UserProfileInfo'
import UserRepos from './UserRepos'

const UserInformation = props => {

  const { user, repos } = props

  return (
    <div>

      <UserProfileInfo user={user} />

      <UserRepos repos={repos} />

    </div>
  )
}

export default UserInformation
