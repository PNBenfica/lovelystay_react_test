import React, { Component } from 'react'
import './App.css'
import AppIntro from './AppIntro'
import UserInformation from './UserInformation'

class App extends Component {

  constructor(props) {
    super(props)
    this.state = { user: {}, userFetched: false, userRepos: [] }
  }

  /*
    helper function to fetch data from url as json
  */
  fetchURLtoJson(url, callback){
    fetch(url).then( response => response.json())
    .then(callback)
  }

  /*
    fetch the user profile data from github
  */
  getUserInformation() {
    const username = 'PNBenfica'
    const url = 'https://api.github.com/users/' + username
    
    this.fetchURLtoJson( url, this.getUserInformationCallback.bind(this))
  }
  
  
  /*
    callback function invoked when user profile is received
  */
	getUserInformationCallback(user) {
    this.displayUserInfo(user)
    this.getUserRepos(user)
  }
  
	displayUserInfo(user) {
    this.setState( { user, userFetched: true } )
	}

  
  /*
    fetch the user repos data from github
  */
	getUserRepos(user) {
    const url = user.repos_url
    this.fetchURLtoJson( url, this.getUserReposCallback.bind(this))
  }

  /*
    callback function invoked when user repos data is received
  */
	getUserReposCallback(userRepos) {
    this.setState( { userRepos } )
  }

  render() {
    const { user, userFetched, userRepos } = this.state

    return (
      <div className="App">

        {
          (userFetched ? 
            
            <UserInformation user={ user } repos={ userRepos } /> :

            <AppIntro getUserInformation={this.getUserInformation.bind(this)} />
          )
        }

      </div>
    )
  }
}

export default App
