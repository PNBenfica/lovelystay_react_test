import React from 'react'

import KeyValueTR from './KeyValueTR'
import UserInformationSection from './UserInformationSection'

const UserProfileInfo = props => {

  const { user } = props

  const userObjectKeys = Object.keys(user)
  
  return (

    <UserInformationSection title="Profile Info">
      <table className="user-info-table">
        <tbody>
          { userObjectKeys.map( (key, i) => <KeyValueTR key={i} k={key} v={user[key]}/> ) }
        </tbody>
      </table>
    </UserInformationSection>
  )
}

export default UserProfileInfo
