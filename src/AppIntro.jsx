import React from 'react'

const AppIntro = props => (

  <div className="App-intro">
    <p>Click on the button to fetch the user information</p>
    <button onClick={props.getUserInformation}>
      Click me
    </button>
  </div>

)

export default AppIntro
