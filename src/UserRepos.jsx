import React from 'react'

import UserInformationSection from './UserInformationSection'
import UserRepoInfo from './UserRepoInfo'

const UserRepos = props => {

  const { repos } = props
  
  return (

    <UserInformationSection title="Repositories">
      <table className="user-info-table">
        { repos.map( (repo, i) =>  <UserRepoInfo key={i} name={ repo.name } description={ repo.description } /> ) }
      </table>
    </UserInformationSection>

  )
}

export default UserRepos
