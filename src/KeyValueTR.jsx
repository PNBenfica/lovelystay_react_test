import React from 'react'

const KeyValueTR = props => {

  const { k, v } = props

  return (

    <tr>
      <td className="key">{ k }:</td>
      <td className="value">{ v }</td>
    </tr>
  )
}

export default KeyValueTR
